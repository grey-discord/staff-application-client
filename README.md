# Grey Staff Application

This is the front-end of the new Grey Staff Application.
Developed using modern technologies such as SvelteKit.

## Developing

Start by installing dependencies:

```sh
npm install
```

Then start the live reloading dev server:

```sh
npm run dev
```

## Building

SvelteKit uses adapters when building for production. This project uses [adapter-node](https://github.com/sveltejs/kit/tree/master/packages/adapter-node).

Compile the client:

```sh
npm run build
```

Then run the client:

```
node build/
```
